SERVER_OUT := "cmd/server"
CLIENT_OUT := "cmd/client"

##API_OUT := "api/api.pb.go"
PB_OUT := pb/*.pb.go
API_REST_OUT := "api/api.pb.gw.go"
API_SWAG_OUT := "api/api.swagger.json"
PKG := "gitlab.com/dhf0820/simple_grpc"
SRC := /Users/dhf/Dropbox/1-work/simple_grpc
SERVER_PKG_BUILD := "${PKG}/cmd/server"
CLIENT_PKG_BUILD := "${PKG}/cmd/client"
PKG_LIST := $(shell go list ${PKG}/... | grep -v /vendor/)

.PHONY: all api server client cert pb1

all: server client

pb1:
    @protoc -I proto proto/*.proto --go_out=plugins=grpc:./pb

#api/api.pb.go: 
api:
	@protoc -I proto \
		-I${GOPATH}/src \
		-I${GOPATH}/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
		--go_out=plugins=grpc:pb \
		proto/simple.proto

# api/api.pb.gw.go: api/api.proto
	# @protoc -I api/ \
		# -I${GOPATH}/src \
		# -I${GOPATH}/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
		# --grpc-gateway_out=logtostderr=true:api \
		# api/api.proto

# api/api.swagger.json: api/api.proto
	# @protoc -I api/ \
		# -I${GOPATH}/src \
		# -I${GOPATH}/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
		# --swagger_out=logtostderr=true:api \
		# api/api.proto

#api: api/api.pb.go  ## Auto-generate grpc go sources
#api: api/api.pb.go api/api.pb.gw.go api/api.swagger.json ## Auto-generate grpc go sources

dep: ## Get the dependencies
	@go get -v -d ./...

tidy: # add all new includes 
	@go mod tidy

server: dep api ## Build the binary file for server
    #CGO_ENABLED=0 GOOS=linux GOARCH=amd64 @go build -a -installsuffix cgo -ldflags="-w -s" -o $(SERVER_OUT) $(SERVER_PKG_BUILD)
	@go build -i -v -o $(SERVER_OUT) $(SERVER_PKG_BUILD)

client: dep api ## Build the binary file for client
	@go build -i -v -o $(CLIENT_OUT) $(CLIENT_PKG_BUILD)

clean: ## Remove previous builds
	@rm $(SERVER_OUT) $(CLIENT_OUT) $(PB_OUT) $(API_REST_OUT) $(API_SWAG_OUT)

help: ## Display this help screen
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

run-server:
	go run cmd/server/main.go -port 7777

run-server-tls:
	go run cmd/server/main.go -port 7777 -tls

run-client:
	go run cmd/client/main.go  -address k8s.vertisoft.com -port 30001


run-client-local:
	go run cmd/client/main.go  -address localhost -port 7777

run-client-linode:
	go run cmd/client/main.go  -address linode1.vertisoft.com -port 7777

run-client-np:
	go run cmd/client/main.go  -address "64.225.89.48" -port 32150
	#30207

run-client-k8s:
	go run cmd/client/main.go  -address "64.225.89.48" -port 80

cert:
	cd cert; ./gen.sh; cd ..