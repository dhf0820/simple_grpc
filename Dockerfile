FROM alpine:latest

ADD cmd/server/simple_grpc ./

EXPOSE 7777
ENTRYPOINT ["./simple_grpc"]