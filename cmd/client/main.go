package main

import (
	"crypto/tls"
	"crypto/x509"
	"flag"
	"fmt"
	"gitlab.com/dhf0820/vs_protobufs/simple/client"
	pb "gitlab.com/dhf0820/vs_protobufs/simple/pb_simple"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"io/ioutil"
	"log"
	"time"
)


const (
	username			= "admin1"
	password  			= "secret"
	refreshDuration 	= 30 * time.Second
)

type SimpleClient struct {
	pb.SimpleClient
}

func NewPingClient(cc *grpc.ClientConn) *SimpleClient {
	service := pb.NewSimpleClient(cc)
	return  &SimpleClient{service}
}

func authMethods() map[string]bool {
	const simplePath = "/simple_grpc.Simple/"
	return map[string]bool{
		simplePath + "SayHello": true,
	}
}

// SayHello generates response to a Ping request
/*func (s *PingClient) SayHello(ctx context.Context, in *pb.PingMessage) (*pb.PingMessage, error) {
	log.Printf("Receive message %s", in.Greeting)
	return &pb.PingMessage{Greeting: "bar"}, nil
}
*/


func loadTLSCredentials() (credentials.TransportCredentials, error) {
	// mutual TLS additions
	// load certificate of the CA who signed servers's certificate
	pemServerCA , err := ioutil.ReadFile("cert/ca-cert.pem")
	if err != nil {
		return nil, err
	}
	certPool := x509.NewCertPool()
	if !certPool.AppendCertsFromPEM(pemServerCA) {
		return nil, fmt.Errorf("failed to add seerver CA's certificate")
	}
	// Load client's certificate and private key
	clientCert, err := tls.LoadX509KeyPair("cert/server-cert.pem", "cert/server-key.pem")
	if err != nil {
		return nil, err
	}

	config := &tls.Config{
		Certificates: 	[]tls.Certificate{clientCert},
		RootCAs:			certPool,
	}
	// Create the Credentials and return it
	return credentials.NewTLS(config), nil
// End of Mutual TLS

	// Create the Credentials and return it
/*	//non mutual TLS
    config := &tls.Config{
		RootCAs: certPool,
	}
*/
}

func main() {
	serverAddress := flag.String("address", "localhost", "the server address without port")
	port := flag.String("port", ":8081", "the server address  port")

	//enableTLS := flag.Bool("tls", false, "enable SSL/TLS")
	flag.Parse()
	sClient, err := client.Connect(*port, *serverAddress, "none")
	if err != nil {
		fmt.Printf("Connect Error: %s\n", err)
		panic(err)
	}
	simpleClient := *sClient
	fmt.Printf("Client connected\n")

	//log.Printf("dial server %s", *serverAddress)
	//
	////var conn *grpc.ClientConn
	//
	//
	//tlsCredentials, err := loadTLSCredentials()
	//if err != nil {
	//	log.Fatal("cannot load TLS credentials")
	//}
	//
	////transportOptions := grpc.WithInsecure()
	//transportOptions := grpc.WithTransportCredentials(tlsCredentials)
	//cc1, err := grpc.Dial(*serverAddress, transportOptions)
	//if err != nil {
	//	log.Fatalf("did not connect: %s", err)
	//}
	////defer cc1.Close()
	//
	//authClient := client.NewAuthClient(cc1, username, password)
	//interceptor, err := client.NewAuthInterceptor(authClient, authMethods(), refreshDuration)
	//if err != nil {
	//	log.Fatal("can not create auth interceptor: ", err)
	//}

	//transportOptions := grpc.WithInsecure()
	//cc2, err := grpc.Dial(
	//	*serverAddress,
	//	transportOptions,
	//	//grpc.WithUnaryInterceptor(interceptor.Unary()),
	//	//grpc.WithStreamInterceptor(interceptor.Stream()),
	//	)
	//if err != nil {
	//	log.Fatal("cannot dial server: ", err)
	//}
	//
	//pClient := client.NewPingClient(cc2)
	//pingClient := *pClient

	//client := pb.NewPingClient(conn)
	startTime := time.Now()
	for i := 1; i < 10; i++ {
		str := fmt.Sprintf("Foo %d", i)
		response, err := simpleClient.SayHello(context.Background(), &pb.SimpleMessage{Greeting: str})
		if err != nil {
			log.Fatalf("Error when calling SayHello: %s", err)
		}
		log.Printf("Response from server: %s", response.Greeting)
		//time.Sleep(1 * time.Second)
	}
	fmt.Printf("Elapsed Time: %s\n", time.Since(startTime))
}

