package main

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"gitlab.com/dhf0820/simple_grpc/internal/service"
	pb "gitlab.com/dhf0820/vs_protobufs/simple/pb_simple"
	"os"

	//"gitlab.com/dhf0820/vs_protobufs/simple/client"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/reflection"
	"io/ioutil"
	"log"
	"net"
	"time"

	"github.com/davecgh/go-spew/spew"
	"google.golang.org/grpc"
)

// Server represents the gRPC server
//type PingServer struct {
//}

func createUser(userStore service.UserStore, username, password, role string) error {
	user, err := service.NewUser(username, password, role)
	if err != nil {
		return err
	}
	return userStore.Save(user)
}
func seedUsers(userStore service.UserStore) error {
	err := createUser(userStore, "admin1", "secret", "admin")
	if err != nil {
		return err
	}
	return createUser(userStore, "user1", "secret", "user")
}

func unaryInterceptor(
	ctx context.Context,
	req interface{},
	info *grpc.UnaryServerInfo,
	handler grpc.UnaryHandler,
) (resp interface{}, err error) {
	log.Println("--> unary interceptor: ", spew.Sdump(info))
	return handler(ctx, req)
}

func streamInterceptor(
	srv interface{},
	ss grpc.ServerStream,
	info *grpc.StreamServerInfo,
	handler grpc.StreamHandler,
) error {
	log.Println("--> Stream interceptor: ", spew.Sdump(info))
	return handler(srv, ss)
}

const (
	secretKey     = "secret"
	tokenDuration = 15 * time.Minute
)

func accessibleRoles() map[string][]string {
	const simplePath = "/simple_grpc.Ping/"
	return map[string][]string {
		simplePath + "SayHello": {"admin"},
	}
}

func loadTLSCredentials() (credentials.TransportCredentials, error) {
	// load certificate of the CA who signed client's certificate
	pemClientCA , err := ioutil.ReadFile("cert/ca-cert.pem")
	if err != nil {
		return nil, err
	}
	certPool := x509.NewCertPool()
	if !certPool.AppendCertsFromPEM(pemClientCA) {
		return nil, fmt.Errorf("failed to add seerver CA's certificate")
	}

	// Load server's certificate and private key
	serverCert, err := tls.LoadX509KeyPair("cert/server-cert.pem", "cert/server-key.pem")
	if err != nil {
		return nil, err
	}

	config := &tls.Config{
		Certificates: 	[]tls.Certificate{serverCert},
		//ClientAuth: 	tls.NoClientCert,                 // not mutual tls
		ClientAuth:     tls.RequireAndVerifyClientCert,   // Mutual TLS
		ClientCAs:      certPool,
	}
	return credentials.NewTLS(config), nil
}


// main start a gRPC server and waits for connection
func main() {
	// create a listener on TCP port 7777
	//lis, err := net.Listen("tcp", fmt.Sprintf(":%d", 7777))
	//if err != nil {
	//	log.Fatalf("failed to listen: %v", err)
	//}

	userStore := service.NewInMemoryUserStore()
	err := seedUsers(userStore)
	if err != nil {
		log.Fatal("Can not seed users")
	}
	//jwtManager := service.NewJWTManager(secretKey, tokenDuration)
	//authServer := service.NewAuthServer(userStore, jwtManager)
	//inteceptor := service.NewAuthInterceptor(jwtManager, accessibleRoles())
	// create a server instance
	ps := service.NewPingServer()
	// create a gRPC server object
	//tlsCredentials, err := loadTLSCredentials()
	//if err != nil {
	//	log.Fatal("cannot load TLS credentials: ", err)
	//}
	grpcServer := grpc.NewServer(
		//grpc.Creds(tlsCredentials),
		//grpc.UnaryInterceptor(inteceptor.Unary()),
		//grpc.StreamInterceptor(inteceptor.Stream()),
	)

	// Reflections register
	reflection.Register(grpcServer)
	// attach the Ping service to the server
	pb.RegisterSimpleServer(grpcServer, ps)
	// start the server
	//pb.RegisterAuthServiceServer(grpcServer, authServer)
	//lis, err := net.Listen("tcp", fmt.Sprintf(":%d", 7777))
	port, exists := os.LookupEnv("RELEASE_PORT")
	//srv, exists := os.LookupEnv("RELEASE_SERVER")
	if !exists {
		port = "7777"
	}
	fmt.Printf("\nsetup listen on port %s\n", port)
	lis, err := net.Listen("tcp", fmt.Sprintf(":%s", port))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	fmt.Printf("Starting to listen on internal tcp port - %s\n", port)
	if err := grpcServer.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %s", err)
	}
}
