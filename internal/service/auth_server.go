package service

import (
	"context"
	"gitlab.com/dhf0820/simple_grpc/pb"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

//AuthServer is the server for authenticating a user to the system
type AuthServer struct {
	userStore  UserStore
	jwtManager *JWTManager
}

//NewAuthServer returns a new AuthServer
func NewAuthServer(userStore UserStore, jwtManager *JWTManager) *AuthServer {
	return &AuthServer{
		userStore:  userStore,
		jwtManager: jwtManager,
	}
}

//Login is a unary RPC to login the user
func (server *AuthServer) Login(ctx context.Context, req *pb.LoginRequest) (*pb.LoginResponse, error) {
	user, err := server.userStore.Find(req.GetUsername())
	if err != nil {
		return nil, status.Errorf(codes.Internal, "incorrect username/password: %v", err)
	}

	if user == nil || !user.IsCorrectPassword(req.GetPassword()) {
		return nil, status.Errorf(codes.NotFound, "incorrect username/password")
	}

	token, err := server.jwtManager.Generate(user)
	if err != nil {
		return nil, status.Errorf(codes.Internal, "can not generate access token")
	}
	res := &pb.LoginResponse{AccessToken: token}
	return res, nil
}
