package service

import (
	"context"
	"fmt"
	pb "gitlab.com/dhf0820/vs_protobufs/simple/pb_simple"
)

type PingServer struct {

}

func NewPingServer() *PingServer {
	return &PingServer{}
}
// SayHello generates response to a Ping request
func (s *PingServer) SayHello(ctx context.Context, in *pb.SimpleMessage) (*pb.SimpleMessage, error) {
	fmt.Printf("Receive message %s\n", in.Greeting)
	return &pb.SimpleMessage{Greeting: "bar - " + in.GetGreeting()}, nil
}

