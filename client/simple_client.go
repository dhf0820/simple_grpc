package client

import (
	"context"
	"fmt"
	pb "gitlab.com/dhf0820/vs_protobufs/simple/pb_simple"
	//"gitlab.com/dhf0820/vs_protobufs/simple/client"
	"google.golang.org/grpc"
)

type SimpleClient struct {
	pb.SimpleClient
}

func NewSimpleClient(cc *grpc.ClientConn) *pb.SimpleClient {
	service := pb.NewSimpleClient(cc)
	return  &service
	//return  &SimpleClient{service}
}

// PingServer is the server API for Ping service.
/*type PingServer interface {
	SayHello(context.Context, *PingMessage) (*PingMessage, error)
}*/


// SayHello generates response to a Ping request
func (s *SimpleClient)Say(ctx context.Context, message string) (string, error) {
	fmt.Printf("Say was called\n")
	pbMessage := &pb.SimpleMessage{
		Greeting: message,
	}
	msg, err := s.SayHello(ctx, pbMessage)
	return msg.Greeting, err
}

//func (s *PingClient) SayHello(ctx context.Context, in *pb.PingMessage) (*pb.PingMessage, error) {
//	log.Printf("Receive message %s", in.Greeting)
//	return &pb.PingMessage{Greeting: "bar client"}, nil
//}

