package client

import (
	"fmt"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
	"log"
	"time"
	"context"
)

// AuthInterceptor is a client interceptor that adds authentication to client calls

type AuthInterceptor struct {
	authClient		*AuthClient
	authMethods		map[string]bool
	accessToken		string
}

// NewAuthIntecceptor returns a new aut interceptor
func NewAuthInterceptor(
	authClient *AuthClient,
	authMethods map[string] bool,
	refreshDuration		time.Duration,
	)(*AuthInterceptor, error) {
		interceptor := &AuthInterceptor {
			authClient: 	authClient,
			authMethods:  	authMethods,
		}

		err := interceptor.scheduleRefreshToken(refreshDuration)
		if err != nil {
			fmt.Printf("scheduleRefreshToken error: %v\n", err)
			return nil, err
		}
		return interceptor, nil
}

func (interceptor *AuthInterceptor) Unary() grpc.UnaryClientInterceptor {
	return func(
		ctx context.Context,
		method string, req,
		reply interface{},
		cc *grpc.ClientConn,
		invoker grpc.UnaryInvoker,
		opts ...grpc.CallOption,
	) error {
		log.Printf("--> unary inteerceptor: %s", method)

		if interceptor.authMethods[method] {
			return invoker(interceptor.attachToken(ctx), method, req, reply, cc, opts...)
		}

		return invoker(ctx, method, req, reply, cc, opts...)
	}
}

func (interceptor *AuthInterceptor) Stream() grpc.StreamClientInterceptor {
	return func(
		ctx context.Context,
		desc *grpc.StreamDesc,
		cc *grpc.ClientConn,
		method string,
		streamer grpc.Streamer,
		opts ...grpc.CallOption,
	) (grpc.ClientStream, error){
		log.Printf("--> stream interceptor: %s", method)

		if interceptor.authMethods[method] {
			return streamer(interceptor.attachToken(ctx), desc, cc, method, opts...)
		}
		return streamer(ctx, desc, cc, method, opts...)
	}

}

func (interceptor *AuthInterceptor) attachToken(ctx context.Context) context.Context {
	return metadata.AppendToOutgoingContext(ctx, "authorization", interceptor.accessToken)
}

func (interceptor *AuthInterceptor) scheduleRefreshToken(refreshDuration time.Duration) error {
	err := interceptor.refresshToken()
	if err != nil {
		// make sure the refresh is available so go routine has a known functioning method
		return err
	}

	go func() {
		wait := refreshDuration
		for {
			time.Sleep(wait)
			err := interceptor.refresshToken()
			if err != nil {
				// wait short time and try again
				wait = time.Second
			} else {
				wait = refreshDuration  // reset the time between resets of token
			}
		}
	}()
	return nil
}

func (interceptor *AuthInterceptor) refresshToken() error {
	accessToken, err := interceptor.authClient.Login()
	if err != nil {
		return err
	}
	interceptor.accessToken = accessToken
	log.Printf("Token Refreshed: accessToken")
	return nil
}