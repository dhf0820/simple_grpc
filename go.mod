module gitlab.com/dhf0820/simple_grpc

go 1.14

replace gitlab.com/dhf0820/simple_grpc => /Users/dhf/Dropbox/1-work/simple_grpc

require (
	github.com/davecgh/go-spew v1.1.1
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/golang/protobuf v1.4.2
	gitlab.com/dhf0820/vs_protobufs v0.0.0-20200725203632-5130b53d2dfa
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9
	golang.org/x/net v0.0.0-20200707034311-ab3426394381
	google.golang.org/grpc v1.30.0
	google.golang.org/protobuf v1.25.0
)
